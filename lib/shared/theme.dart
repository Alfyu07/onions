part of 'shared.dart';

Color mainColor = "37C641".toColor();
Color greyColor = "8D92A3".toColor();
Color whiteColor = "EFFBF0".toColor();
Color blackColor = "0B280D".toColor();
Color yellowColor = "FFC700".toColor();

Widget loadingIndicator = SpinKitFadingCircle(
  size: 45,
  color: mainColor,
);

TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
TextStyle blackFontStyle1 = GoogleFonts.poppins()
    .copyWith(color: blackColor, fontSize: 22, fontWeight: FontWeight.w500);
TextStyle blackFontStyle2 = GoogleFonts.poppins()
    .copyWith(color: blackColor, fontSize: 16, fontWeight: FontWeight.w500);
TextStyle blackFontStyle3 = GoogleFonts.poppins().copyWith(
  color: blackColor,
);

const double defaultMargin = 24;
