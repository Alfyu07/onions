part of 'models.dart';

enum FoodType { new_food, popular, recommended }

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;
  final List<FoodType> types;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate,
      this.types = const []});

  factory Food.fromJson(Map<String, dynamic> data) => Food(
      id: data['id'],
      picturePath: data['picturePath'],
      name: data['name'],
      description: data['description'],
      ingredients: data['ingredients'],
      price: data['price'],
      rate: (data['rate'] as num).toDouble(),
      types: data['types'].toString().split(',').map((e) {
        switch (e) {
          case 'recommended':
            return FoodType.recommended;
            break;
          case 'popular':
            return FoodType.popular;
            break;
          default:
            return FoodType.new_food;
        }
      }).toList());

  @override
  List<Object> get props =>
      [id, picturePath, name, description, ingredients, price, rate];
}

List<Food> mockFoods = [
  Food(
    id: 1,
    name: "Kahangga",
    description:
        "Jika sedang mencari oleh-oleh khas Bima, maka Kahangga adalah pilihan yang tepat. Sebab, kue ini hanya bisa ditemukan di Bima. Nama kahangga sendiri berasal dari bentuknya yang retak.",
    ingredients: "tepung beras, gula, telur, garam",
    price: 30000,
    rate: 4.8,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Kahangga-bima-585x380.jpg",
    types: [FoodType.new_food, FoodType.recommended, FoodType.popular],
  ),
  Food(
    id: 2,
    name: "Uta Sepi Tumis",
    description:
        "Makanan ini akan sangat cocok bagi para pecinta seafood dan pedas. Rasa asam pedas dari uta sepi tumis sangat cocok disantap bersama dengan nasi hangat. Ketika mengunjungi Bima Nusa Tenggara Barat wajib untuk mencoba santapan ini dengan resep aslinya.",
    ingredients: "Rdang rebon, Tomat, Cabai, Kemangi, Asam muda",
    price: 40000,
    rate: 4.6,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Uta-Sepi-Tumis-bima-585x380.jpg",
    types: [FoodType.new_food],
  ),
  Food(
    id: 3,
    name: "Uta Palumara Londe (Bandeng Kuah Santan)",
    description:
        "ikan bandeng dengan perpaduan rasa asam, pedas, dan manis ini juga dilengkapi dengan wangi daun kemangi. Dijamin menambah rasa nikmat saat disantap dan meningkatkan selera makan. Uta palumara londe dapat ditemukan dengan mudah di warung makan sekitaran Bima.",
    ingredients:
        "Ikan bandeng, bawang merah, bawang putih, tomat, cabai, kunyit",
    price: 45000,
    rate: 5,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Uta-Palumara-Londe-bima-585x380.jpg",
    types: [FoodType.new_food, FoodType.recommended, FoodType.popular],
  ),
  Food(
    id: 4,
    name: "Bingka Dolu",
    description:
        "Bingka Dolu merupakan salah satu kue khas Bima yang tidak boleh dilewatkan. Kue ini memiliki tekstur lembut dan kenyal. Kue berbentuk bunga ini sangat disukai oleh masyarakat sehingga dapat ditemui dengan mudah baik di pedagang di jalanan maupun pasar tradisional.",
    ingredients: "tepung terigu, gula, santan, garam, telur",
    price: 25000,
    rate: 4.7,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Bingka-Dolu-585x380.jpg",
    types: [FoodType.recommended],
  ),
  Food(
    id: 5,
    name: "Sayur Sambi",
    description:
        "Daun kesambi dibentuk menjadi bulatan. Ro’o sambi ini memiliki banyak khasiat dengan rasanya yang unik sehingga sangat wajib untuk dicoba.",
    ingredients: "Daun kesambi",
    price: 15000,
    rate: 4.2,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Sayur-Sambi-bima-585x380.jpg",
    types: [FoodType.new_food],
  ),
  Food(
    id: 6,
    name: "Kue Mata Pisang",
    description:
        "Biasanya makanan khas Bima sekitar menikmati kue mata pisang sebagai cemilan dengan taburan es serut sehingga lebih segar terutama ketika disantap siang hari.",
    ingredients: "Ubi kayu, Pisang kepok, gula, garam, kelapa parut",
    price: 20000,
    rate: 4.9,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Kue-Mata-Pisang-bima-585x380.jpg",
    types: [FoodType.popular],
  ),
  Food(
    id: 7,
    name: "Kahangga",
    description:
        "Jika sedang mencari oleh-oleh khas Bima, maka Kahangga adalah pilihan yang tepat. Sebab, kue ini hanya bisa ditemukan di Bima. Nama kahangga sendiri berasal dari bentuknya yang retak.",
    ingredients: "tepung beras, gula, telur, garam",
    price: 30000,
    rate: 4.8,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Kahangga-bima-585x380.jpg",
    types: [FoodType.new_food],
  ),
  Food(
    id: 8,
    name: "Uta Maju Puru",
    description:
        "Jika biasanya dendeng terbuat dari daging sapi, di Bima justru terbuat dari daging rusa. Sehingga dapat memberi pengalaman baru bagi yang belum pernah memakan daging rusa.",
    ingredients: "Dendeng rusa, Rempah rempah rahasia",
    price: 80000,
    rate: 4,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Uta-Maju-Puru-bima-585x380.jpg",
    types: [FoodType.recommended, FoodType.popular],
  ),
  Food(
    id: 9,
    name: "Tota Fo’o",
    description:
        "Sambal yang sangat unik dengan perpaduan rasa asam pedasnya karena terbuat dari buah mangga dan cabai. Rasanya yang unik tentu akan membuat siapapun ketagihan.",
    ingredients: "Mangga, Cabai, Gula, Garam",
    price: 30000,
    rate: 4.9,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Tota-Foo-bima-585x380.jpg",
    types: [FoodType.recommended, FoodType.popular],
  ),
  Food(
    id: 10,
    name: "Pangaha Bunga",
    description:
        "Pangaha bunga ini sangat cocok juga dibawa sebagai oleh-oleh ketika berkunjung ke Bima. Sebab, dapat disimpan dalam waktu lama sehingga tidak perlu khawatir basi. Pangaha bunga ini cukup mirip dengan kue lontar di Jawa. Pembeda antara lentari dengan pangaha bunga hanya dari jumlah kelopaknya saja.",
    ingredients: "Tepung beras ketan",
    price: 5000,
    rate: 4.5,
    picturePath:
        "https://makananoleholeh.com/wp-content/uploads/2020/10/Pangaha-Bunga-bima-585x380.jpg",
    types: [FoodType.new_food],
  )
];
