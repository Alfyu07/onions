import 'package:flutter/material.dart';
import 'package:onion/models/models.dart';
import 'package:onion/shared/shared.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:supercharged/supercharged.dart';
import 'package:flutter_svg/flutter_svg.dart';

part 'svg_icon.dart';
part 'custom_bottom_navbar.dart';
part 'rating_stars.dart';
part 'food_card.dart';
part 'custom_tabbar.dart';
part 'food_list_item.dart';
part 'order_list_item.dart';
