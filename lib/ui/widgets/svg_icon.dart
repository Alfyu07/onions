part of 'widgets.dart';

class SvgIcon extends StatelessWidget {
  final SvgPicture svg;
  final Function onTap;
  final double padding;
  final double size;

  const SvgIcon(this.svg,
      {Key key, this.padding = 8, this.onTap, this.size = 32.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (onTap != null) ? onTap : () {},
      child: Container(
          padding: EdgeInsets.all(padding),
          alignment: Alignment.center,
          width: size,
          height: size,
          child: Stack(
            children: [svg],
          )),
    );
  }
}
