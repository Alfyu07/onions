part of 'widgets.dart';

class CustomBottomNavBar extends StatelessWidget {
  final int selectedIndex;
  final Function(int index) onTap;

  CustomBottomNavBar({this.selectedIndex = 0, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: double.infinity,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap(0);
              }
            },
            child: (selectedIndex == 0)
                ? Container(
                    width: 48,
                    height: 48,
                    // padding: EdgeInsets.all(4),
                    child: SvgIcon(
                      SvgPicture.asset(
                        'assets/house-fill.svg',
                        color: mainColor,
                        width: 48,
                      ),
                      size: 48,
                    ),
                  )
                : Container(
                    width: 48,
                    height: 48,
                    // padding: EdgeInsets.all(4),
                    child: SvgIcon(
                      SvgPicture.asset(
                        'assets/house.svg',
                        color: greyColor,
                        width: 48,
                      ),
                      size: 48,
                    ),
                  ),
          ),
          GestureDetector(
            onTap: () {
              this.onTap(1);
            },
            child: (selectedIndex == 1)
                ? SvgIcon(
                    SvgPicture.asset('assets/shopping-bag-open-fill.svg',
                        color: mainColor, width: 48),
                    size: 48)
                : SvgIcon(
                    SvgPicture.asset('assets/shopping-bag-open.svg',
                        color: greyColor, width: 48),
                    size: 48),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap(2);
              }
            },
            child: (selectedIndex == 2)
                ? SvgIcon(
                    SvgPicture.asset('assets/user-fill.svg',
                        color: mainColor, width: 48),
                    size: 48)
                : SvgIcon(
                    SvgPicture.asset('assets/user.svg',
                        color: greyColor, width: 48),
                    size: 48),
          )
        ],
      ),
    );
  }
}
